const inputIconChange = function (event){
    if (event.classList.contains('fa-eye')) {
        event.classList.replace('fa-eye', 'fa-eye-slash')
        event.parentElement.querySelector('input').type = 'text'
    }else {
        event.classList.replace('fa-eye-slash', 'fa-eye')
        event.parentElement.querySelector('input').type = 'password'
    }
}

const buttonFunc = function (event){
    const inputArr = document.getElementsByClassName('input-pass')
    if (inputArr[0].value != inputArr[1].value){
        event.preventDefault()
        const wrongPass = document.createElement("span");
        wrongPass.textContent = 'Нужно ввести одинаковые значения'
        wrongPass.style.color = 'red'
        document.body.append(wrongPass)
    } else (alert('You are welcome'))
}

const form = document.getElementById('form')

form.addEventListener('click', function (event){
    let eventI = event.target.closest('i')
    let eventBtn = event.target.closest('button')
    if (eventI){inputIconChange(eventI)}
    if (eventBtn){buttonFunc(event)}
})